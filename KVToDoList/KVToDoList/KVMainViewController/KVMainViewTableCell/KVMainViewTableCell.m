//
//  KVMainViewTableCell.m
//  KVToDoList
//
//  Created by kvak on 14.03.16.
//  Copyright © 2016 Andre Kvashuk. All rights reserved.
//

#import "KVMainViewTableCell.h"
#import <QuartzCore/QuartzCore.h>
#import <UIKit/UIKit.h>
#import "KVTask.h"

@implementation KVMainViewTableCell

@synthesize textLabel;

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier {
    
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    
    if (self) {
        self.taskLabelsView = [KVTaskLabelsView new];
        [self addSubview:self.taskLabelsView];
        
        self.deleteButton.backgroundColor = [UIColor whiteColor];
        self.deleteButton.layer.cornerRadius = 10.f;
        self.deleteButton.layer.borderColor = [UIColor blackColor].CGColor;
        self.deleteButton.titleLabel.textColor = [UIColor blackColor];
        [self addSubview:self.deleteButton];
        
        self.statusButton = [KVStatusButton new];
        [self addSubview:self.statusButton];
        
        [self.statusButton addTarget:self
                              action:@selector(buttonAction)
                    forControlEvents: UIControlEventTouchUpInside];
        
    }
    return self;
}

//- (void)configureWithbject:(id)object {
//    KVTask *task = object;
//}
#pragma  mark - UIElements configuration

- (void)buttonAction {
    if (self.someBlock) {
        self.someBlock();
    }
}
//
//- (void)configureDeleteButton {
//    self.deleteButton.backgroundColor = [UIColor whiteColor];
//    self.deleteButton.layer.cornerRadius = 10.f;
//    self.deleteButton.layer.borderColor = [UIColor blackColor].CGColor;
//    self.deleteButton.titleLabel.textColor = [UIColor blackColor];
//}
//



#pragma mark - LayoutSubviews

- (void)layoutSubviews {
    [super layoutSubviews];
    
    static NSInteger const defaultSpace = 5;
    CGRect cellFrame = self.bounds;

    self.statusButton.frame = CGRectMake(cellFrame.origin.x + defaultSpace,
                                         cellFrame.origin.y + defaultSpace,
                                         cellFrame.size.width/3,
                                         cellFrame.size.height - defaultSpace);
    
    self.taskLabelsView.frame =
                        CGRectMake(CGRectGetMaxX(self.statusButton.frame) + defaultSpace,
                        cellFrame.origin.y + defaultSpace,
                        cellFrame.size.width - self.statusButton.frame.size.width - (defaultSpace*3),
                        cellFrame.size.height - defaultSpace);
}

@end
