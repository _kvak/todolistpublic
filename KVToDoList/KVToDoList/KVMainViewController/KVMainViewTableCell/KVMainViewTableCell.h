//
//  KVMainViewTableCell.h
//  KVToDoList
//
//  Created by kvak on 14.03.16.
//  Copyright © 2016 Andre Kvashuk. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "KVStatusButton.h"
#import "KVLabel.h"
#import "KVTaskLabelsView.h"

@interface KVMainViewTableCell : UITableViewCell

@property (nonatomic, copy) void (^someBlock)(void);
@property (nonatomic, strong) KVStatusButton *statusButton;
@property (nonatomic, strong) KVTaskLabelsView *taskLabelsView;
@property (nonatomic, strong) UIButton *deleteButton;
@property (nonatomic, assign) BOOL isSwipedLeft;

- (void)configureWithbject:(id)object;

@end
