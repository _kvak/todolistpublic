//
//  KVMainViewController.m
//  KVToDoList
//
//  Created by kvak on 12.03.16.
//  Copyright © 2016 Andre Kvashuk. All rights reserved.
//

#import "KVMainViewController.h"
#import "KVStatusButton.h"
#import "KVTask.h"
#import "KVMainViewTableCell.h"


@interface KVMainViewController() <UITableViewDataSource, UITableViewDelegate>

@property (nonatomic, strong) NSMutableArray *statusButtons;

@end


@implementation KVMainViewController

@dynamic view;

static NSString *const cellUniqueIdentifier = @"cellUniqueIdentifier";
static NSString *const kvMainViewTableCellUniqueIdentifier = @"kvMainViewTableCellUniqueIdentifier";
static NSString *const createButtonTitle = @"Create";
static NSString *const viewControllerTitle = @"ToDo list";

#pragma mark Life Cycle

- (void)loadView {
    [super loadView];
    
    self.view = [KVMainView new];
}

- (void)viewDidLoad {
//    UINavigationItem *NavB÷utton

    self.view.mainTableView.dataSource = self;
    self.view.mainTableView.delegate = self;
    self.tasks = [NSMutableArray new];
    self.statusButtons = [NSMutableArray new];
    [self buttonPreparations];
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:YES];
    
    [self sortTasksByStatus];
    [self.view.mainTableView reloadData];
    
    NSLog(@"%@", self.tasks);
}
#pragma mark - Preparations

- (void)buttonPreparations {
    
    UIBarButtonItem *createButton  = [[UIBarButtonItem alloc] initWithTitle:createButtonTitle
                                                                      style:UIBarButtonItemStylePlain
                                                                     target:self
                                                                     action:@selector(goToTaskEditView)];
    
    self.navigationItem.rightBarButtonItem = createButton;
    self.title = viewControllerTitle;
    [self.navigationItem setTitle:viewControllerTitle];
    
    
    UISwipeGestureRecognizer *leftSwipe = [[UISwipeGestureRecognizer alloc]
                                           initWithTarget:self
                                           action:@selector(swipeLeftAction:)];
    
    [leftSwipe setDirection:UISwipeGestureRecognizerDirectionLeft];
    
    
    [self.view.mainTableView addGestureRecognizer:leftSwipe];

}

#pragma mark - KVMainTableView Life Cycle

- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        [self.tasks removeObjectAtIndex:indexPath.row];
        [self.view.mainTableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationLeft];
    }
}

#pragma mark - UITableViewDataSource methods

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.tasks.count;
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

#pragma mark - Table Cells methods

- (UITableViewCell*)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    KVMainViewTableCell *cell = [tableView dequeueReusableCellWithIdentifier:kvMainViewTableCellUniqueIdentifier];
    
    if (cell == nil) {
        cell = [[KVMainViewTableCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:kvMainViewTableCellUniqueIdentifier];
    }
    
    KVTask *tempTask = self.tasks[indexPath.row];
    cell.taskLabelsView.taskContent.text = tempTask.content;
    [cell.taskLabelsView.taskOwner setText:tempTask.owner];
    cell.statusButton.status = (ButtonState)tempTask.status;
    
    __weak KVMainViewTableCell *blockCell = cell;
    
    cell.someBlock = ^{
        [self changeTaskStatus:blockCell.statusButton task:tempTask];
        
        NSIndexSet *sections = [NSIndexSet indexSetWithIndexesInRange:NSMakeRange(0, [self.view.mainTableView numberOfSections])];
            [self sortTasksByStatus];
            [self.view.mainTableView reloadSections:sections withRowAnimation:UITableViewRowAnimationFade];
 
   
    };
    
    
    return cell;
}

- (void)changeTaskStatus:(id)sender task:(KVTask*)task  {
    KVStatusButton* button = (KVStatusButton*)sender;
    
    [task incrementStatus];
    ButtonState status = (ButtonState)task.status;
    
    [button setStatus:status];
}

#pragma mark - Gestures

//- (void)swipeLeftAction:(UISwipeGestureRecognizer*)gesture {
//    KVMainViewTableCell *swipedCell = [self getSwipeGestureActivatedTableViewCell:gesture];
//    CGPoint swipeLocation = [gesture locationInView:gesture.view];
//    NSIndexPath *swipedIndexPath = [self.view.mainTableView indexPathForRowAtPoint:swipeLocation];
//    
//    if (!(swipedCell.isSwipedLeft) && gesture.direction == UISwipeGestureRecognizerDirectionLeft) {
//        [self tableView:(UITableView *)self.view.mainTableView commitEditingStyle:UITableViewCellEditingStyleDelete forRowAtIndexPath:(NSIndexPath *)swipedIndexPath];
//
//        NSLog(@"swipe left action");
//        swipedCell.isSwipedLeft = YES;
//    }
//}

// Gets the gesture activated cell in the tableView

//- (KVMainViewTableCell*)getSwipeGestureActivatedTableViewCell:(UISwipeGestureRecognizer*) gesture{
//    CGPoint swipeLocation = [gesture locationInView:gesture.view];
//    NSIndexPath *swipedIndexPath = [self.view.mainTableView indexPathForRowAtPoint:swipeLocation];
//    KVMainViewTableCell *swipedCell = [self.view.mainTableView cellForRowAtIndexPath:swipedIndexPath];
//    
//    return swipedCell;
//}
//#pragma mark - Touches
//- (void)pressesBegan:(NSSet<UIPress *> *)presses withEvent:(nullable UIPressesEvent *)event {
//    NSLog(@"presses began");
//}
//- (void)touchesBegan:(NSSet<UITouch *> *)touches withEvent:(nullable UIEvent *)event {
//    
//    NSLog(@"touches began");
//    
//}
//- (void)touchesMoved:(NSSet<UITouch *> *)touches withEvent:(nullable UIEvent *)event {
//    
//     NSLog(@"touches moved");
//    
//}
//- (void)touchesEnded:(NSSet<UITouch *> *)touches withEvent:(nullable UIEvent *)event {
//    
//     NSLog(@"touches ended");
//    
//}
//- (void)touchesCancelled:(nullable NSSet<UITouch *> *)touches withEvent:(nullable UIEvent *)event {
//    
//     NSLog(@"touches cancelled");
//}
#pragma mark - Actions

- (void)goToTaskEditView {
    KVTaskEditViewController *taskEditViewController = [KVTaskEditViewController new];
    
    taskEditViewController.delegate = self;
    [self.navigationController pushViewController:taskEditViewController
                                         animated:YES];
  
}

- (void)deleteTaskAtButtonClicked:(UIButton*) sender {

    CGPoint buttonLocation = sender.frame.origin;
    NSIndexPath *clickedIndexPath = [self.view.mainTableView indexPathForRowAtPoint:buttonLocation];
   
    NSArray *indexPathArray = @[clickedIndexPath];
    
    [self.view.mainTableView deleteRowsAtIndexPaths:indexPathArray withRowAnimation:UITableViewRowAnimationLeft];
    [self.tasks removeObjectAtIndex:clickedIndexPath.row];
    [self.view.mainTableView reloadData];
}

//- (NSMutableArray *)sortTasksByStatus {
//    NSMutableArray *sortedTaskArray;
//    sortedTaskArray = [self.tasks sorted:^NSComparisonResult(KVTask *one, KVTask *two) {
//        NSInteger *first  = one.status;
//        NSInteger *second = two.status;
//        
//        return [first compare:second];
//    }];
//}

- (void)sortTasksByStatus {
    NSSortDescriptor *taskSortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"status" ascending:YES];
    NSMutableArray *taskSortDescriptorsArray = [NSMutableArray arrayWithObject:taskSortDescriptor];
    
    [self.tasks sortUsingDescriptors:taskSortDescriptorsArray];
}

#pragma mark KVTaskEditViewControllerDelegate method

- (void)taskEditController:(KVTaskEditViewController *)taskEditController didCreateTask:(KVTask *)task {
    [self.tasks addObject:task];
}


@end
