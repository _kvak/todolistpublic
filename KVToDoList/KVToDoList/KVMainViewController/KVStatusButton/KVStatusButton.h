//
//  KVStatusButton.h
//  KVToDoList
//
//  Created by kvak on 15.03.16.
//  Copyright © 2016 Andre Kvashuk. All rights reserved.
//

#import <UIKit/UIKit.h>

typedef NS_ENUM(NSInteger, ButtonState) {
    ButtonStateToDo,
    ButtonStateInProgress,
    ButtonStateDone
};
@class KVTask;
//@class KVMainViewTableCell;

@interface KVStatusButton : UIButton

@property (nonatomic, assign) ButtonState status;

@end
