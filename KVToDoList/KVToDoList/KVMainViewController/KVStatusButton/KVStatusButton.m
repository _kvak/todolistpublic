//
//  KVStatusButton.m
//  KVToDoList
//
//  Created by kvak on 15.03.16.
//  Copyright © 2016 Andre Kvashuk. All rights reserved.
//

#import "KVStatusButton.h"
#import "KVTask.h"

typedef NSInteger (^ButtonAction)(KVTask*);

@interface KVStatusButton()

@end

@implementation KVStatusButton

- (instancetype)init {
    self = [super init];
    if (self) {
        self.backgroundColor = [UIColor redColor];
        self.layer.cornerRadius = 10.f;
        self.layer.borderColor = [UIColor whiteColor].CGColor;
        
        [self setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
        [self setTitle:@"To Do" forState:UIControlStateNormal];
        [self addSubview:self.titleLabel];
    }
    return self;
}

- (void)setStatus:(ButtonState)status {
    switch (status) {
           
        case KVTaskStatusInProgress:
             NSLog(@"set status progress");
            [self setTitle:@"In progress" forState:UIControlStateNormal];
            self.backgroundColor = [UIColor yellowColor];
            break;
        case KVTaskStatusToDo:
            [self setTitle:@"To Do" forState:UIControlStateNormal];
            self.backgroundColor= [UIColor redColor];
            break;
        case KVTaskStatusDone:
            [self setTitle:@"Done" forState:UIControlStateNormal];
            self.backgroundColor = [UIColor greenColor];
            break;
        default:
           self.titleLabel.text = @"To Do";
            break;
    }
     NSLog(@"set status end");
    _status = status;
}
@end
