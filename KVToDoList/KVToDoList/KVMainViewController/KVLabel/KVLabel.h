//
//  KVLabel.h
//  KVToDoList
//
//  Created by kvak on 19.03.16.
//  Copyright © 2016 Andre Kvashuk. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface KVLabel : UILabel

- (CGRect)textRectForBounds:(CGRect)bounds limitedToNumberOfLines:(NSInteger)numberOfLines;
- (void)drawTextInRect:(CGRect)rect;

@end
