//
//  KVLabel.m
//  KVToDoList
//
//  Created by kvak on 19.03.16.
//  Copyright © 2016 Andre Kvashuk. All rights reserved.
//

#import "KVLabel.h"

@implementation KVLabel

- (instancetype)init
{
    self = [super init];
    if (self) {
        self.textColor = [UIColor whiteColor];
    }
    return self;
}
- (CGRect)textRectForBounds:(CGRect)bounds limitedToNumberOfLines:(NSInteger)numberOfLines {
    return CGRectInset(bounds, 5, 2);
}

- (void)drawTextInRect:(CGRect)rect {
    UIEdgeInsets textInset = UIEdgeInsetsMake(2, 5, 2, 5);
    
    [super drawTextInRect:UIEdgeInsetsInsetRect(rect, textInset)];
}

@end
