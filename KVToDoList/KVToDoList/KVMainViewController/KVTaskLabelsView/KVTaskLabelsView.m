//
//  KVTaskLabelsView.m
//  KVToDoList
//
//  Created by kvak on 19.03.16.
//  Copyright © 2016 Andre Kvashuk. All rights reserved.
//

#import "KVTaskLabelsView.h"

#define CORNER_RADIUS 10

@implementation KVTaskLabelsView

- (instancetype)init
{
    self = [super init];
    if (self) {
        self.layer.cornerRadius = CORNER_RADIUS;
        self.layer.backgroundColor = [UIColor yellowColor].CGColor;

        
        [self configureOwnerLabel];
        [self addSubview:self.taskOwner];
        
        [self configureTextLabel];
        [self addSubview:self.taskContent];
    }
    return self;
}

#pragma mark - Configure elements

- (void)configureOwnerLabel {
    self.taskOwner = [KVLabel new];
    self.taskOwner.textColor = [UIColor redColor];
    self.taskOwner.adjustsFontSizeToFitWidth = YES;
    self.taskOwner.layer.cornerRadius = CORNER_RADIUS;
    
}

- (void)configureTextLabel {
    self.taskContent = [KVLabel new];
    self.taskContent.textColor = [UIColor blackColor];
    self.taskContent.lineBreakMode = NSLineBreakByWordWrapping;
    self.taskContent.numberOfLines = 0;
    self.taskContent.adjustsFontSizeToFitWidth = YES;
    self.taskContent.layer.cornerRadius = CORNER_RADIUS;
}

#pragma mark - Layout Subviews

- (void)layoutSubviews {
    [super layoutSubviews];
    CGRect taskLabelsViewBounds = self.bounds;
    

    self.taskOwner.frame = CGRectMake(
                                      taskLabelsViewBounds.origin.x + CORNER_RADIUS/2,
                                      taskLabelsViewBounds.origin.y,
                                      taskLabelsViewBounds.size.width - CORNER_RADIUS,
                                      taskLabelsViewBounds.size.height/2
                                      );
    
    self.taskContent.frame = CGRectMake(
                                        taskLabelsViewBounds.origin.x + CORNER_RADIUS/2,
                                        CGRectGetMaxY(self.taskOwner.frame),
                                        taskLabelsViewBounds.size.width - CORNER_RADIUS,
                                        taskLabelsViewBounds.size.height/2
                                        );
  

    
}

@end
