//
//  KVTaskLabelsView.h
//  KVToDoList
//
//  Created by kvak on 19.03.16.
//  Copyright © 2016 Andre Kvashuk. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "KVLabel.h"

@interface KVTaskLabelsView : UIView

@property (nonatomic, strong) KVLabel *taskOwner;
@property (nonatomic, strong) KVLabel *taskContent;

@end
