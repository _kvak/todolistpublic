//
//  KVMainView.m
//  KVToDoList
//
//  Created by kvak on 12.03.16.
//  Copyright © 2016 Andre Kvashuk. All rights reserved.
//


#import "KVMainView.h"

@implementation KVMainView

- (instancetype)init {
    self = [super init];
    if (self) {
        self.mainTableView = [UITableView new];
        self.mainTableView.backgroundColor = [UIColor whiteColor];
        [self addSubview:self.mainTableView];
    
        self.backgroundColor = [UIColor greenColor];
//        self.mainTableView.allowsSelection = NO;
    }
    return self;
}

- (void)layoutSubviews {
    [super layoutSubviews];
    
    self.mainTableView.frame = self.bounds;
}

@end
