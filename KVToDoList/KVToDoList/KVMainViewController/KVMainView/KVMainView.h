//
//  KVMainView.h
//  KVToDoList
//
//  Created by kvak on 12.03.16.
//  Copyright © 2016 Andre Kvashuk. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "KVMainViewTableCell.h"

@interface KVMainView : UIView

@property (nonatomic, strong) UITableView *mainTableView;
//@property (nonatomic, strong) UIBarButtonItem *createButton;


@end
