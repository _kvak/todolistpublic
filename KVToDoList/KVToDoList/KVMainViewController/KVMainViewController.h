//
//  KVMainViewController.h
//  KVToDoList
//
//  Created by kvak on 12.03.16.
//  Copyright © 2016 Andre Kvashuk. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "KVTaskEditViewController.h"
#import "KVMainView.h"


@interface KVMainViewController : UIViewController <KVTaskEditViewControllerDelegate>

@property (nonatomic, strong) KVMainView *view;
@property (nonatomic, strong) UIBarButtonItem *createButton;
@property (nonatomic, strong) NSMutableArray *tasks;

@end
