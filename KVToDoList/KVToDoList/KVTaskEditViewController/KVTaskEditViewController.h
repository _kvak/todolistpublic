//
//  KVTaskEditViewController.h
//  KVToDoList
//
//  Created by kvak on 12.03.16.
//  Copyright © 2016 Andre Kvashuk. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "KVTaskEditView.h"


@class KVTask;
@class KVTaskEditViewController;

@protocol KVTaskEditViewControllerDelegate <NSObject>

- (void)taskEditController:(KVTaskEditViewController *)taskEditController didCreateTask:(KVTask *)task;

@end


@interface KVTaskEditViewController : UIViewController

@property (nonatomic,strong) KVTaskEditView *view;
@property (nonatomic, weak) id <KVTaskEditViewControllerDelegate> delegate;

@end



