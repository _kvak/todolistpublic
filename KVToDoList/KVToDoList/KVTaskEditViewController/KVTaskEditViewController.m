//
//  KVTaskEditViewController.m
//  KVToDoList
//
//  Created by kvak on 12.03.16.
//  Copyright © 2016 Andre Kvashuk. All rights reserved.
//
#import "KVTask.h"
#import "KVTaskEditViewController.h"


#pragma mark - String Const
static NSString *const kDoneNavButtonTitle           = @"Done";
static NSString *const kDoneAlertControllerMessage   = @"The task has been created!";
static NSString *const kDoneAlertControllerTitle     = @"Success";
static NSString *const kOkTitle                      = @"OK";
static NSString *const kEmptyTaskTitle               = @"Error";
static NSString *const kEmptyTaskMessage             = @"Can't create an empty task";
static NSString *const kCloseTitle                   = @"Close";


@interface KVTaskEditViewController() <UIPickerViewDataSource, UIPickerViewDelegate>

@property (nonatomic, strong) NSArray *taskOwners;
@property (nonatomic, assign) BOOL taskCreated;
@end

@implementation KVTaskEditViewController

@dynamic view;

#pragma mark - Life Cycle

- (void)loadView {
    self.view = [KVTaskEditView new];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.view.taskOwner.dataSource = self;
    self.view.taskOwner.delegate = self;
    self.taskOwners = @[@"John", @"Michael", @"Bill"];
    self.taskCreated = NO;
    [self navigationButtonsPreparation];


}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    
     NSNotificationCenter *notificationCenter = [NSNotificationCenter defaultCenter];
    
    [ notificationCenter addObserver:self
                 selector:@selector(handleKeyboardWillShowNotification:)
                     name:UIKeyboardWillShowNotification
                   object:nil];
    
    [notificationCenter addObserver:self
                selector:@selector(handleKeyboardWillHideNotification:)
                    name:UIKeyboardWillHideNotification
                  object:nil];
    
}

- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
}

- (void)dealloc {
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}


#pragma mark - Preparations

- (void)navigationButtonsPreparation {
    
    UIBarButtonItem *doneNavButton = [[UIBarButtonItem alloc]
                                      initWithTitle:@"Done"
                                        style:UIBarButtonItemStyleDone
                                            target:self
                                            action:@selector(createTask)];
    
    [self.navigationItem setRightBarButtonItem:doneNavButton animated:YES];
}
- (void)showAlertWithMessage:(NSString*)alertMessage {
    UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"Alert"
                                                                   message:alertMessage
                                                            preferredStyle:UIAlertControllerStyleAlert];
    UIAlertAction *action = [UIAlertAction actionWithTitle:@"OK"
                                                     style:UIAlertActionStyleCancel
                                                   handler:nil];
    [alert addAction:action];
    [self presentViewController:alert animated:YES completion:nil];
}

#pragma mark - Actions

- (void)createTask {
    if (!_taskCreated) {
        NSString *content = self.view.contentTextView.text;
        NSInteger row = [self.view.taskOwner selectedRowInComponent:0];
        NSString *owner = [self.taskOwners objectAtIndex:row];
        
        if (![content  isEqual: @""]) {
            KVTask *newTask = [[KVTask alloc] init];
            newTask.owner = owner;
            newTask.content = content;
            NSLog(@"%@", [newTask description]);
            [self.view.contentTextView resignFirstResponder];
            self.view.contentTextView.editable = NO;
            
            [self.delegate taskEditController:self didCreateTask:newTask];
            [self showAlertWithMessage:@"Task created"];
            _taskCreated = YES;
        } else {
            [self showAlertWithMessage:@"Cant create empty task"];
        }
    }

}


- (void)emptyFields {
    self.view.contentTextView.text = @"";
}

#pragma mark - TextField methods



#pragma mark - UIPickerViewDataSource methods

- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView {
    return 1;
}

- (NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component {
    return [self.taskOwners count];
}

#pragma mark - UIPickerViewDelegate methods

- (nullable NSString *)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component  {
    return self.taskOwners[row];
}
#pragma mark - Keyboard Event Notifications

- (void)handleKeyboardWillShowNotification:(NSNotification *) notification {
    
    NSDictionary *userInfo = notification.userInfo;
    
//    Get info on the duration of the animation
//    NSTimeInterval timeInterval = [userInfo [UIKeyboardAnimationDurationUserInfoKey] doubleValue];
//    UIViewAnimationOptions animationCurve = [userInfo [UIKeyboardAnimationCurveUserInfoKey] integerValue];
    
////    getting keyboard frame and translating it into the view coordinates
//    CGRect keyboardScreenFrameBegin = [userInfo[UIKeyboardFrameBeginUserInfoKey] CGRectValue];
//    CGRect keyboardScreenFrameEnd   = [userInfo[UIKeyboardFrameEndUserInfoKey] CGRectValue];
////    converting the keyboard frame into the view
//    CGRect keyboardViewFrameBegin  = [self.view convertRect:keyboardScreenFrameBegin fromView:self.view.window];
//    CGRect keyboardViewFrameEnd    = [self.view convertRect:keyboardScreenFrameEnd fromView:self.view.window];

    CGSize kbSize = [[userInfo objectForKey:UIKeyboardFrameBeginUserInfoKey] CGRectValue].size;
    
    UIEdgeInsets newIndicatorInset = UIEdgeInsetsMake(0, 0, kbSize.height, 0);
    
    self.view.contentInset = newIndicatorInset;
    self.view.scrollIndicatorInsets = newIndicatorInset;
    
    CGRect aRect = self.view.frame;
     aRect.size.height -= kbSize.height;
    
    if (CGRectContainsPoint(aRect, self.view.contentTextView.frame.origin)) {
        CGPoint scrollPoint = CGPointMake(0.0f, self.view.contentTextView.frame.origin.y - kbSize.height + self.view.taskOwner.frame.size.height);
        [self.view setContentOffset:scrollPoint animated:YES];
    }
    
}
- (void)handleKeyboardWillHideNotification:(NSNotification *)notification {
  
    UIEdgeInsets contentInsets = UIEdgeInsetsZero;
    
    self.view.contentInset = contentInsets;
    self.view.scrollIndicatorInsets = contentInsets;
}



@end
