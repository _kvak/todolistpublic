//
//  KVTask.h
//  KVToDoList
//
//  Created by kvak on 12.03.16.
//  Copyright © 2016 Andre Kvashuk. All rights reserved.
//

#import <Foundation/Foundation.h>

typedef NS_ENUM(NSInteger, KVTaskStatus) {
    KVTaskStatusToDo,
    KVTaskStatusInProgress,
    KVTaskStatusDone
};

@interface KVTask : NSObject

@property (nonatomic, strong) NSString *owner;
@property (nonatomic, strong) NSString *content;
@property (nonatomic, assign) KVTaskStatus status;

//- (instancetype)initWithOwnerAndContent:(NSString*)taskOwner and:(NSString*)taskContent;
- (void)incrementStatus;

@end
