//
//  KVTask.m
//  KVToDoList
//
//  Created by kvak on 12.03.16.
//  Copyright © 2016 Andre Kvashuk. All rights reserved.
//

#import "KVTask.h"

@implementation KVTask

//- (instancetype)initWithOwnerAndContent:(NSString*)taskOwner and:(NSString*)taskContent {
//    self = [super init];
//    
//    if (self) {
//        self.content = taskContent;
//        self.owner = taskOwner;
//        self.status = ToDo;
//    }
//    return self;
//}
- (instancetype)init {
    self = [super init];
    
    if (self) {
        self.content = @"random content";
        self.owner = @"random owner";
        self.status = KVTaskStatusToDo;
    }
    return self;
}

- (void)incrementStatus {
    self.status += 1;
    
    if (self.status > KVTaskStatusDone) {
        self.status = KVTaskStatusToDo;
    }
}

- (NSString *)description {
    return [NSString stringWithFormat:@"%@ => %@", self.owner, self.content];
}

//
//- (NSComparisonResult)compare:(KVTask *)otherTask {
//    return [self.status compare:otherTask.status];
//}

@end
