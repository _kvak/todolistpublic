//
//  KVTaskEditView.h
//  KVToDoList
//
//  Created by kvak on 12.03.16.
//  Copyright © 2016 Andre Kvashuk. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface KVTaskEditView : UIScrollView

@property (nonatomic, strong) UITextView *contentTextView;
@property (nonatomic, strong) UITextField *taskOwnerTextField;
@property (nonatomic, strong) UIPickerView *taskOwner;


@end
