//
//  KVTaskEditView.m
//  KVToDoList
//
//  Created by kvak on 12.03.16.
//  Copyright © 2016 Andre Kvashuk. All rights reserved.
//

#import "KVTaskEditView.h"

static NSString *const placeholder       = @"Task description goes here";
static NSString *const createButtonTitle = @"Create";

@interface KVTaskEditView()

@property (nonatomic, strong) UIButton *createButton;

@end

@implementation KVTaskEditView

- (instancetype)init {
    self = [super init];
    if (self) {
        self.backgroundColor = [UIColor grayColor];
        
        [self configureTaskOwnerTextField];
        [self addSubview:self.taskOwnerTextField];
        
        [self configureContentTextView];
        [self addSubview:self.contentTextView];
        
        [self configureCreateButton];
        [self addSubview:self.createButton];
        
        self.taskOwner = [UIPickerView new];
        self.taskOwner.hidden = YES;
        [self addSubview:self.taskOwner];
        
    }
    return self;
}

- (void)configureContentTextView {
    self.contentTextView = [UITextView new];
    self.contentTextView.backgroundColor = [UIColor whiteColor];
    self.contentTextView.layer.cornerRadius = 10;
}

- (void)configureCreateButton {
    self.createButton = [UIButton new];
    [self.createButton.titleLabel setText:createButtonTitle];
}

- (void)configureTaskOwnerTextField {
    self.taskOwnerTextField = [UITextField new];
    self.taskOwnerTextField.layer.cornerRadius = 10;
    self.taskOwnerTextField.inputView = self.taskOwner;
    
    
}
- (void)layoutSubviews {
    [super layoutSubviews];
    
    CGRect mainRect = self.bounds;
    
    static const int kDefaultHeight = 44;
    static const int kDefaultInset = 5;
    
    int kTaskOwnerWidth = mainRect.size.width/3;
    static const int kTaskOwnerHeight = 44;
    
    
    self.contentTextView.frame = CGRectMake(kDefaultInset,
                                      self.frame.size.height/2 - self.contentTextView.frame.size.height/2,
                                     self.frame.size.width - kDefaultInset*2,
                                      kDefaultHeight);
    
    self.taskOwnerTextField.frame = CGRectMake(self.contentTextView.frame.origin.x,
                                              self.contentTextView.frame.origin.y - self.contentTextView.frame.size.height - kDefaultHeight, kTaskOwnerWidth,
                                                    kTaskOwnerHeight);
   
}
- (BOOL)textViewShouldBeginEditing:(UITextView *)textView {
    [self showTaskOwnerPickerView];
    self.taskOwner.hidden = NO;
    
    return NO;
}
- (void)showTaskOwnerPickerView {
    
    self.taskOwner.frame = CGRectMake( 0,
                                      CGRectGetMaxY(self.contentTextView.frame),
                                      self.frame.size.width,
                                      self.frame.size.height/6);
}


@end
